import * as fetch from 'node-fetch'

/**
 * helper that fetches the url provided and does a simple status check
 * before returning the json
 *
 * @param {String} url
 *
 * @return {Object} json
 */
const fetchUser = (url) => {
  return fetch(url).then((res) => {
    // if (res.status >= 400) {
    //   throw new Error('Server responding with error!')
    // }
    if (res.status === 403) {
      throw new Error(
        '403 - Rate Limit Exceeded! Read more: <a href="https://developer.github.com/v3/#rate-limiting" style="color: blue">Rate Limiting</a>'
      )
    }
    if (res.status === 422) {
      throw new Error(
        '422 - Unprocessable Entity: Only 1000 first searches are available'
      )
    }
    return res.json()
  })
}

export { fetchUser }
