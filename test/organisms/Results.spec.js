import { shallowMount } from '@vue/test-utils'
import Results from '@/components/organisms/Results'

describe('Results', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Results, {
      propsData: {
        results: {
          items: [{}, {}, {}, {}, {}]
        }
      }
    })
  })

  it('updates the page', () => {
    const page = { value: 5 }
    wrapper.vm.updatePage(page)
    expect(wrapper.emitted('update:results')).toBeTruthy()
  })
})
