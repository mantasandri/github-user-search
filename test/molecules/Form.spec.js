import { shallowMount } from '@vue/test-utils'
import Form from '@/components/molecules/Form'

describe('Form', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Form)
  })

  it('updates the input', () => {
    const string = { value: 'Testing 123' }
    wrapper.vm.updateInput(string)
    expect(wrapper.vm.$data.input).toBe(string.value)
  })

  it('triggers submit', () => {
    wrapper.vm.triggerSubmit()
    expect(wrapper.emitted('submit')).toBeTruthy()
  })
})
