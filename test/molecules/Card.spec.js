import { shallowMount } from '@vue/test-utils'
import Card from '@/components/molecules/Card'

describe('Card', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Card, {
      propsData: {
        item: {
          login: 'test'
        }
      }
    })
  })
  it('gets user info', async () => {
    await wrapper.vm.fetchUserInfo()
    expect(wrapper.vm.userInfo.login).toContain('test')
  })
})
