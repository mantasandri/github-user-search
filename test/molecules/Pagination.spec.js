import { shallowMount } from '@vue/test-utils'
import Pagination from '@/components/molecules/Pagination'

describe('Pagination', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Pagination)
    const scrollSpy = jest.fn()
    window.scroll = scrollSpy
  })

  describe('computed properties', () => {
    describe('determine range', () => {
      it('should return the results from 1 to the max buttons visible', () => {
        wrapper.setProps({ totals: 100 })
        expect(wrapper.vm.range.end).toBe(5)
      })

      it('should move to the current page, while retaining end page', () => {
        wrapper.setProps({
          totals: 300,
          currentPage: 2
        })
        expect(wrapper.vm.range.end).toBe(5)
      })

      it('should move to the current page after moving past midway point, while retaining end page', () => {
        wrapper.setProps({
          totals: 300,
          currentPage: 7
        })
        expect(wrapper.vm.range.start).toBe(1)
      })

      it('should stop moving current page', () => {
        wrapper.setProps({
          totals: 300,
          currentPage: 12
        })
        expect(wrapper.vm.range.end).toBe(15)
      })
    })

    it('should return beginning pages', () => {
      wrapper.setProps({
        currentPage: 10,
        totals: 500
      })
      const pages = [1, 2]
      expect(wrapper.vm.beginningPages).toStrictEqual(pages)
    })
    it('should return last pages', () => {
      wrapper.setProps({
        totals: 500
      })
      const pages = [49, 50]
      expect(wrapper.vm.lastPages).toStrictEqual(pages)
    })
  })

  describe('test methods', () => {
    it('should trigger prev page', () => {
      wrapper.vm.triggerPrev()
      expect(wrapper.vm.localPage).toBe(0)
    })
    it('should trigger prev page', () => {
      wrapper.vm.triggerNext()
      expect(wrapper.vm.localPage).toBe(2)
    })
    it('should trigger correct page', () => {
      wrapper.vm.triggerPage(5)
      expect(wrapper.vm.localPage).toBe(5)
    })
  })
})
