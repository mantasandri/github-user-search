import { shallowMount } from '@vue/test-utils'
import Select from '@/components/atoms/Select'

describe('Select', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Select)
  })
  it('has select option', () => {
    expect(wrapper.contains('select')).toBe(true)
  })
})
