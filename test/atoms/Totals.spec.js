import { mount } from '@vue/test-utils'
import Totals from '@/components/atoms/Totals'

describe('Totals', () => {
  let wrapper
  beforeEach(() => {
    wrapper = mount(Totals, {
      propsData: {
        results: { total_count: 500, items: [{}, {}, {}, {}, {}] }
      }
    })
  })

  it('should return the correct beginning index', () => {
    expect(wrapper.vm.indexStart).toBe(1)
  })

  it('should return this correct index on page 2', () => {
    wrapper.setProps({
      page: 2
    })
    expect(wrapper.vm.indexStart).toBe(10)
  })

  it('should return the end of the index', () => {
    expect(wrapper.vm.indexEnd).toBe(5)
  })

  it('should return this end index on page 2', () => {
    wrapper.setProps({
      page: 2
    })
    expect(wrapper.vm.indexEnd).toBe(10)
  })

  it('should calculate the total pages', () => {
    expect(wrapper.vm.totalPages).toBe(50)
  })

  it('should display correct string', () => {
    const string = 'Displaying 1-5 of 50 pages.  Total results of 500'
    expect(wrapper.vm.string).toBe(string)
  })
})
