import { shallowMount } from '@vue/test-utils'
import Button from '@/components/atoms/Button'

describe('Button', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Button, {
      propsData: {
        name: 'button'
      }
    })
  })
  it('has a button', () => {
    expect(wrapper.contains('button')).toBe(true)
  })
})
