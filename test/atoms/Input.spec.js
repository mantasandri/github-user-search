import { shallowMount } from '@vue/test-utils'
import Input from '@/components/atoms/Input'

describe('Input', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(Input)
  })

  it('has input button', () => {
    expect(wrapper.contains('input')).toBe(true)
  })

  it('watcher should trigger emit', () => {
    wrapper.vm.$data.inputValue = 'testing'
    expect(wrapper.vm.$data.isLoading).toBeFalsy()
  })

  it('should update input event when typed', () => {
    wrapper.vm.updateInput('testing')
    expect(wrapper.emitted('input:selected')).toBeTruthy()
  })

  it('should submit event when submitted', () => {
    wrapper.vm.onSubmit()
    expect(wrapper.emitted('input:submit')).toBeTruthy()
  })

  it('should trigger a search', async (done) => {
    wrapper.vm.$data.inputValue = 'testing'
    await wrapper.vm.fetchQuery()
    expect(wrapper.vm.isLoading).toBeTruthy()
    done()
  })
})