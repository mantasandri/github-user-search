import { shallowMount } from '@vue/test-utils'
import index from '@/pages/index'

describe('index', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(index)
  })

  it('has a button', () => {
    expect(wrapper.contains('button')).toBe(false)
  })
})
